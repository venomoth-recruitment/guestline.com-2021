# Guestline recruitment task

The repository contains solution for [Guestline](https://www.guestline.com/) recruitment task. The description of the task is available in [task.md](./task.md).

## Used technologies

The project is a console application written in C#/.NET Core 3.1.

## Requirements

To build and run the project you need to have .NET Core 3.1 SDK installed on your system.

## Run instructions

### Start

To start the game run the following command from project's root folder:

```shell
dotnet run --project src/Game.Console.Single
```

### Unit Tests

To run tests run the following command from project's root folder:

```shell
dotnet test
```

## Board Legend

![Board Legend](docs/board_legend.png)
