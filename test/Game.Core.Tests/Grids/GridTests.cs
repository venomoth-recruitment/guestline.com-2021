using System;
using System.Collections.Generic;
using Xunit;
using Game.Core.Grids;

namespace Game.Core.Tests.Grids
{
    public class GridTests
    {
        [Fact]
        public void GetLocations_ShouldReturnValuesForEveryGridLocation()
        {
            var locationOne = new Location(0, 0);
            var locationTwo = new Location(0, 1);
            var locationThree = new Location(1, 0);
            var locationFour = new Location(0, 1);
            var gridSize = new GridSize(2, 2);

            var sut = new Grid(gridSize);
            var locations = sut.GetLocations();

            Assert.Equal(4, locations.Count);
            Assert.True(locations.ContainsKey(locationOne));
            Assert.True(locations.ContainsKey(locationTwo));
            Assert.True(locations.ContainsKey(locationThree));
            Assert.True(locations.ContainsKey(locationFour));
        }

        [Fact]
        public void GetLocations_ShouldReturnAllLocationsAsFreeAfterCreation()
        {
            var gridSize = new GridSize(5, 5);

            var sut = new Grid(gridSize);
            var locations = sut.GetLocations();

            foreach (var location in locations)
            {
                Assert.Equal(GridLocationState.Free, location.Value);
            }
        }

        [Fact]
        public void GetLocations_ShouldReturnCopy()
        {
            var changedLocation = new Location(0, 0);
            var gridSize = new GridSize(5, 5);

            var sut = new Grid(gridSize);
            var locationsBeforeChange = sut.GetLocations();
            locationsBeforeChange[changedLocation] = GridLocationState.Hit;
            var locationsAfterChange = sut.GetLocations();

            Assert.Equal(25, locationsAfterChange.Count);
            Assert.Equal(GridLocationState.Free, locationsAfterChange[changedLocation]);
        }

        [Fact]
        public void SetLocationState_ShouldChangeStateOfGivenLocation()
        {
            var changedLocation = new Location(2, 3);
            var gridSize = new GridSize(5, 5);

            var sut = new Grid(gridSize);
            var sutAfterChange = sut.SetLocationState(changedLocation, GridLocationState.Miss);
            var locations = sutAfterChange.GetLocations();

            Assert.Equal(25, locations.Count);
            Assert.Equal(GridLocationState.Miss, locations[changedLocation]);

            foreach (var location in locations)
            {
                if (!location.Key.Equals(changedLocation))
                {
                    Assert.Equal(GridLocationState.Free, location.Value);
                }
            }
        }

        [Fact]
        public void SetLocationState_ShouldThrowExceptionWhenLocationDoesNotExist()
        {
            var changedLocation = new Location(4, 5);
            var gridSize = new GridSize(5, 5);

            var sut = new Grid(gridSize);

            var exception = Assert.Throws<ArgumentException>(() => sut.SetLocationState(changedLocation, GridLocationState.Ship));
            Assert.Equal("Location does not exist.", exception.Message);
        }

        [Fact]
        public void SetLocationState_ShouldReturnCopyOfGrid()
        {
            var changedLocation = new Location(0, 0);
            var gridSize = new GridSize(5, 5);

            var sut = new Grid(gridSize);
            var sutAfterChange = sut.SetLocationState(changedLocation, GridLocationState.Miss);

            Assert.False(sut.GetLocations().Equals(sutAfterChange.GetLocations()));
        }

        [Fact]
        public void SetLocationsState_ShouldChangeStateOfGivenLocations()
        {
            var changedLocationOne = new Location(1, 3);
            var changedLocationTwo = new Location(0, 4);
            var changedLocationThree = new Location(0, 0);
            var changedStateOne = GridLocationState.Sunk;
            var changedStateTwo = GridLocationState.Ship;
            var changedStateThree = GridLocationState.Hit;
            var changedLocationsState = new Dictionary<Location, GridLocationState> {
                { changedLocationOne, changedStateOne },
                { changedLocationTwo, changedStateTwo },
                { changedLocationThree, changedStateThree }
            };
            var gridSize = new GridSize(5, 5);

            var sut = new Grid(gridSize);
            var sutAfterChange = sut.SetLocationsState(changedLocationsState);
            var locations = sutAfterChange.GetLocations();

            Assert.Equal(25, locations.Count);
            Assert.Equal(GridLocationState.Sunk, locations[changedLocationOne]);
            Assert.Equal(GridLocationState.Ship, locations[changedLocationTwo]);
            Assert.Equal(GridLocationState.Hit, locations[changedLocationThree]);

            foreach (var location in locations)
            {
                if (
                    !location.Key.Equals(changedLocationOne)
                    && !location.Key.Equals(changedLocationTwo)
                    && !location.Key.Equals(changedLocationThree)
                )
                {
                    Assert.Equal(GridLocationState.Free, location.Value);
                }
            }
        }

        [Fact]
        public void SetLocationsState_ShouldThrowExceptionWhenLocationDoesNotExist()
        {
            var changedLocationOne = new Location(4, 1);
            var changedLocationTwo = new Location(5, 2);
            var changedStateOne = GridLocationState.Miss;
            var changedStateTwo = GridLocationState.Ship;
            var changedLocationsState = new Dictionary<Location, GridLocationState> {
                { changedLocationOne, changedStateOne },
                { changedLocationTwo, changedStateTwo }
            };
            var gridSize = new GridSize(5, 5);

            var sut = new Grid(gridSize);

            var exception = Assert.Throws<ArgumentException>(() => sut.SetLocationsState(changedLocationsState));
            Assert.Equal("Location does not exist.", exception.Message);
        }

        [Fact]
        public void SetLocationsState_ShouldReturnCopyOfGrid()
        {
            var changedLocationOne = new Location(0, 4);
            var changedLocationTwo = new Location(1, 3);
            var changedStateOne = GridLocationState.Hit;
            var changedStateTwo = GridLocationState.Miss;
            var changedLocationsState = new Dictionary<Location, GridLocationState> {
                { changedLocationOne, changedStateOne },
                { changedLocationTwo, changedStateTwo }
            };
            var changedLocation = new Location(0, 0);
            var gridSize = new GridSize(5, 5);

            var sut = new Grid(gridSize);
            var sutAfterChange = sut.SetLocationsState(changedLocationsState);

            Assert.False(sut.GetLocations().Equals(sutAfterChange.GetLocations()));
        }
    }
}
