using System.Collections.Generic;
using Moq;
using Xunit;
using Game.Core.Fleets;
using Game.Core.Grids;
using Game.Core.Players;
using Game.Core.Ships;

namespace Game.Core.Tests
{
    public class GameTests
    {
        Dictionary<string, IGrid> _grids = new Dictionary<string, IGrid>();

        [Fact]
        public void Start_ShouldMarkShipsOnGrids()
        {
            var gridSize = new GridSize(3, 3);
            var playerOneShipLocation = new Location(0, 1);
            var playerTwoShipLocation = new Location(1, 0);

            var playerOneShipMock = new Mock<IShip>();
            playerOneShipMock.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { playerOneShipLocation });

            var playerTwoShipMock = new Mock<IShip>();
            playerTwoShipMock.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { playerTwoShipLocation });

            var playerOneFleetMock = new Mock<IFleet>();
            playerOneFleetMock.Setup(fleet => fleet.GetShip(playerOneShipLocation)).Returns(playerOneShipMock.Object);
            playerOneFleetMock.Setup(fleet => fleet.GetShips()).Returns(new List<IShip>() { playerOneShipMock.Object });

            var playerTwoFleetMock = new Mock<IFleet>();
            playerTwoFleetMock.Setup(fleet => fleet.GetShip(playerTwoShipLocation)).Returns(playerTwoShipMock.Object);
            playerTwoFleetMock.Setup(fleet => fleet.GetShips()).Returns(new List<IShip>() { playerTwoShipMock.Object });
            playerTwoFleetMock.Setup(fleet => fleet.IsSunk).Returns(true);

            var playerOneMock = new Mock<IPlayer>();
            playerOneMock.Setup(player => player.GetFleet(gridSize)).Returns(playerOneFleetMock.Object);
            playerOneMock.Setup(player => player.GetShot(It.IsAny<IGrid>(), It.IsAny<IGrid>())).Returns(playerTwoShipLocation);

            var playerTwoMock = new Mock<IPlayer>();
            playerTwoMock.Setup(player => player.GetFleet(gridSize)).Returns(playerTwoFleetMock.Object);

            var sut = new Game(gridSize, playerOneMock.Object, playerTwoMock.Object);
            sut.Start();

            playerOneMock.Verify(player => player.GetShot(
                It.Is<IGrid>(grid => this.AddGrid("playerOneOceanGrid", grid)),
                It.Is<IGrid>(grid => this.AddGrid("playerOneTargetGrid", grid))
            ));

            playerTwoMock.Verify(player => player.ShowWaitForOpponentShot(
                It.Is<IGrid>(grid => this.AddGrid("playerTwoOceanGrid", grid)),
                It.Is<IGrid>(grid => this.AddGrid("playerTwoTargetGrid", grid))
            ));

            var playerOneOceanGridLocations = this._grids["playerOneOceanGrid"].GetLocations();
            var playerOneTargetGridLocations = this._grids["playerOneTargetGrid"].GetLocations();
            var playerTwoOceanGridLocations = this._grids["playerTwoOceanGrid"].GetLocations();
            var playerTwoTargetGridLocations = this._grids["playerTwoTargetGrid"].GetLocations();

            Assert.Equal(GridLocationState.Ship, playerOneOceanGridLocations[playerOneShipLocation]);
            Assert.Equal(GridLocationState.Ship, playerTwoOceanGridLocations[playerTwoShipLocation]);

            foreach (var location in playerOneOceanGridLocations)
            {
                if (!location.Key.Equals(playerOneShipLocation))
                {
                    Assert.Equal(GridLocationState.Free, location.Value);
                }
            }

            foreach (var location in playerTwoOceanGridLocations)
            {
                if (!location.Key.Equals(playerTwoShipLocation))
                {
                    Assert.Equal(GridLocationState.Free, location.Value);
                }
            }

            foreach (var location in playerOneTargetGridLocations)
            {
                Assert.Equal(GridLocationState.Free, location.Value);
            }

            foreach (var location in playerTwoTargetGridLocations)
            {
                Assert.Equal(GridLocationState.Free, location.Value);
            }
        }

        [Fact]
        public void Start_ShouldCallPlayersMethodsInRightSequenceWithRightValues()
        {
            var gridSize = new GridSize(3, 3);

            var playerOneShipLocationOne = new Location(0, 0);
            var playerOneShipLocationTwo = new Location(0, 1);
            var playerTwoShipLocationOne = new Location(1, 0);
            var playerTwoShipLocationTwo = new Location(1, 1);

            var playerOneMissedShot = new Location(0, 1);
            var playerTwoMissedShot = new Location(1, 1);

            var playerOneShipMock = new Mock<IShip>();
            playerOneShipMock.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { playerOneShipLocationOne, playerOneShipLocationTwo });
            playerOneShipMock.Setup(ship => ship.TryHit(playerOneShipLocationOne)).Returns(true);

            var playerTwoShipMock = new Mock<IShip>();
            playerTwoShipMock.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { playerTwoShipLocationOne, playerTwoShipLocationTwo });
            playerTwoShipMock.Setup(ship => ship.TryHit(playerTwoShipLocationOne)).Returns(true);
            playerTwoShipMock.Setup(ship => ship.TryHit(playerTwoShipLocationTwo)).Returns(true);

            var playerOneFleetMock = new Mock<IFleet>();
            playerOneFleetMock.Setup(fleet => fleet.GetShips()).Returns(new List<IShip>() { playerOneShipMock.Object });
            playerOneFleetMock.Setup(fleet => fleet.GetShip(playerTwoMissedShot)).Returns(playerOneShipMock.Object);
            playerOneFleetMock.Setup(fleet => fleet.GetShip(playerOneShipLocationOne)).Returns(playerOneShipMock.Object);
            playerOneFleetMock.SetupSequence(fleet => fleet.IsSunk)
                .Returns(false)
                .Returns(false);

            var playerTwoFleetMock = new Mock<IFleet>();
            playerTwoFleetMock.Setup(fleet => fleet.GetShips()).Returns(new List<IShip>() { playerTwoShipMock.Object });
            playerTwoFleetMock.Setup(fleet => fleet.GetShip(playerOneMissedShot)).Returns((IShip)null);
            playerTwoFleetMock.Setup(fleet => fleet.GetShip(playerTwoShipLocationOne)).Returns(playerTwoShipMock.Object);
            playerTwoFleetMock.Setup(fleet => fleet.GetShip(playerTwoShipLocationTwo)).Returns(playerTwoShipMock.Object);
            playerTwoFleetMock.SetupSequence(fleet => fleet.IsSunk)
                .Returns(false)
                .Returns(false)
                .Returns(true);

            var playerOneMock = new Mock<IPlayer>();
            var playerTwoMock = new Mock<IPlayer>();

            var sequence = new MockSequence();
            playerOneMock.InSequence(sequence).Setup(player => player.GetFleet(gridSize)).Returns(playerOneFleetMock.Object);
            playerTwoMock.InSequence(sequence).Setup(player => player.GetFleet(gridSize)).Returns(playerTwoFleetMock.Object);
            playerTwoMock.InSequence(sequence).Setup(player => player.ShowWaitForOpponentShot(It.IsAny<IGrid>(), It.IsAny<IGrid>()));
            playerOneMock.InSequence(sequence).Setup(player => player.GetShot(It.IsAny<IGrid>(), It.IsAny<IGrid>())).Returns(playerOneMissedShot);
            playerOneMock.InSequence(sequence).Setup(player => player.ShowMissedShot(It.IsAny<IGrid>(), It.IsAny<IGrid>()));
            playerTwoMock.InSequence(sequence).Setup(player => player.ShowOpponentMissedShot(It.IsAny<IGrid>(), It.IsAny<IGrid>()));
            playerOneMock.InSequence(sequence).Setup(player => player.ShowWaitForOpponentShot(It.IsAny<IGrid>(), It.IsAny<IGrid>()));
            playerTwoMock.InSequence(sequence).Setup(player => player.GetShot(It.IsAny<IGrid>(), It.IsAny<IGrid>())).Returns(playerTwoMissedShot);
            playerTwoMock.InSequence(sequence).Setup(player => player.ShowMissedShot(It.IsAny<IGrid>(), It.IsAny<IGrid>()));
            playerOneMock.InSequence(sequence).Setup(player => player.ShowOpponentMissedShot(It.IsAny<IGrid>(), It.IsAny<IGrid>()));
            playerTwoMock.InSequence(sequence).Setup(player => player.ShowWaitForOpponentShot(It.IsAny<IGrid>(), It.IsAny<IGrid>()));
            playerOneMock.InSequence(sequence).Setup(player => player.GetShot(It.IsAny<IGrid>(), It.IsAny<IGrid>())).Returns(playerTwoShipLocationOne);
            playerOneMock.InSequence(sequence).Setup(player => player.ShowHitShot(It.IsAny<IGrid>(), It.IsAny<IGrid>(), It.IsAny<string>()));
            playerTwoMock.InSequence(sequence).Setup(player => player.ShowOpponentHitShot(It.IsAny<IGrid>(), It.IsAny<IGrid>(), It.IsAny<string>()));
            playerOneMock.InSequence(sequence).Setup(player => player.ShowWaitForOpponentShot(It.IsAny<IGrid>(), It.IsAny<IGrid>()));
            playerTwoMock.InSequence(sequence).Setup(player => player.GetShot(It.IsAny<IGrid>(), It.IsAny<IGrid>())).Returns(playerOneShipLocationOne);
            playerTwoMock.InSequence(sequence).Setup(player => player.ShowHitShot(It.IsAny<IGrid>(), It.IsAny<IGrid>(), It.IsAny<string>()));
            playerOneMock.InSequence(sequence).Setup(player => player.ShowOpponentHitShot(It.IsAny<IGrid>(), It.IsAny<IGrid>(), It.IsAny<string>()));
            playerTwoMock.InSequence(sequence).Setup(player => player.ShowWaitForOpponentShot(It.IsAny<IGrid>(), It.IsAny<IGrid>()));
            playerOneMock.InSequence(sequence).Setup(player => player.GetShot(It.IsAny<IGrid>(), It.IsAny<IGrid>())).Returns(playerTwoShipLocationTwo);
            playerOneMock.InSequence(sequence).Setup(player => player.ShowShipSunk(It.IsAny<IGrid>(), It.IsAny<IGrid>(), It.IsAny<string>()));
            playerTwoMock.InSequence(sequence).Setup(player => player.ShowOpponentShipSunk(It.IsAny<IGrid>(), It.IsAny<IGrid>(), It.IsAny<string>()));
            playerTwoMock.InSequence(sequence).Setup(player => player.ShowDefeat());
            playerOneMock.InSequence(sequence).Setup(player => player.ShowVictory());


            var sut = new Game(gridSize, playerOneMock.Object, playerTwoMock.Object);
            sut.Start();
        }

        [Fact]
        public void Start_ShouldShowPlayerOneVictoryAndPlayerTwoDefeatWhenPlayerOneWins()
        {
            var gridSize = new GridSize(3, 3);
            var playerOneShipLocation = new Location(0, 1);
            var playerTwoShipLocation = new Location(1, 0);

            var playerOneShipMock = new Mock<IShip>();
            playerOneShipMock.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { playerOneShipLocation });

            var playerTwoShipMock = new Mock<IShip>();
            playerTwoShipMock.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { playerTwoShipLocation });

            var playerOneFleetMock = new Mock<IFleet>();
            playerOneFleetMock.Setup(fleet => fleet.GetShip(playerOneShipLocation)).Returns(playerOneShipMock.Object);
            playerOneFleetMock.Setup(fleet => fleet.GetShips()).Returns(new List<IShip>() { playerOneShipMock.Object });

            var playerTwoFleetMock = new Mock<IFleet>();
            playerTwoFleetMock.Setup(fleet => fleet.GetShip(playerTwoShipLocation)).Returns(playerTwoShipMock.Object);
            playerTwoFleetMock.Setup(fleet => fleet.GetShips()).Returns(new List<IShip>() { playerTwoShipMock.Object });
            playerTwoFleetMock.Setup(fleet => fleet.IsSunk).Returns(true);

            var playerOneMock = new Mock<IPlayer>();
            playerOneMock.Setup(player => player.GetFleet(gridSize)).Returns(playerOneFleetMock.Object);
            playerOneMock.Setup(player => player.GetShot(It.IsAny<IGrid>(), It.IsAny<IGrid>())).Returns(playerTwoShipLocation);

            var playerTwoMock = new Mock<IPlayer>();
            playerTwoMock.Setup(player => player.GetFleet(gridSize)).Returns(playerTwoFleetMock.Object);

            var sut = new Game(gridSize, playerOneMock.Object, playerTwoMock.Object);
            sut.Start();

            playerOneMock.Verify(player => player.ShowVictory(), Times.Exactly(1));
            playerTwoMock.Verify(player => player.ShowDefeat(), Times.Exactly(1));
        }

        [Fact]
        public void Start_ShouldShowPlayerTwoVictoryAndPlayerOneDefeatWhenPlayerTwoWins()
        {
            var gridSize = new GridSize(3, 3);
            var playerOneShipLocation = new Location(0, 1);
            var playerTwoShipLocation = new Location(1, 0);

            var playerOneShipMock = new Mock<IShip>();
            playerOneShipMock.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { playerOneShipLocation });

            var playerTwoShipMock = new Mock<IShip>();
            playerTwoShipMock.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { playerTwoShipLocation });

            var playerOneFleetMock = new Mock<IFleet>();
            playerOneFleetMock.Setup(fleet => fleet.GetShip(playerOneShipLocation)).Returns(playerOneShipMock.Object);
            playerOneFleetMock.Setup(fleet => fleet.GetShips()).Returns(new List<IShip>() { playerOneShipMock.Object });
            playerOneFleetMock.Setup(fleet => fleet.IsSunk).Returns(true);

            var playerTwoFleetMock = new Mock<IFleet>();
            playerTwoFleetMock.Setup(fleet => fleet.GetShip(playerTwoShipLocation)).Returns(playerTwoShipMock.Object);
            playerTwoFleetMock.Setup(fleet => fleet.GetShips()).Returns(new List<IShip>() { playerTwoShipMock.Object });
            playerTwoFleetMock.Setup(fleet => fleet.IsSunk).Returns(false);

            var playerOneMock = new Mock<IPlayer>();
            playerOneMock.Setup(player => player.GetFleet(gridSize)).Returns(playerOneFleetMock.Object);
            playerOneMock.Setup(player => player.GetShot(It.IsAny<IGrid>(), It.IsAny<IGrid>())).Returns(playerOneShipLocation);

            var playerTwoMock = new Mock<IPlayer>();
            playerTwoMock.Setup(player => player.GetFleet(gridSize)).Returns(playerTwoFleetMock.Object);
            playerTwoMock.Setup(player => player.GetShot(It.IsAny<IGrid>(), It.IsAny<IGrid>())).Returns(playerOneShipLocation);

            var sut = new Game(gridSize, playerOneMock.Object, playerTwoMock.Object);
            sut.Start();

            playerOneMock.Verify(player => player.ShowDefeat(), Times.Exactly(1));
            playerTwoMock.Verify(player => player.ShowVictory(), Times.Exactly(1));
        }

        // TODO Write more tests

        private bool AddGrid(string name, IGrid grid)
        {
            this._grids.Add(name, grid);

            return true;
        }
    }
}
