using System.Collections.Generic;
using System.Linq;
using Moq;
using Xunit;
using Game.Core.Fleets;
using Game.Core.Grids;
using Game.Core.Players;

namespace Game.Core.Tests.Players
{
    public class AiPlayerTests
    {
        [Fact]
        public void GetShot_ShouldReturnEveryGridLocationWithoutDuplications()
        {
            var fleetBuilderMock = new Mock<IFleetBuilder>();
            var oceanGridMock = new Mock<IGrid>();
            var targetGridMock = new Mock<IGrid>();
            var gridSize = new GridSize(7, 9);

            var expectedLocations = new List<Location>();
            for (var column = 0; column < gridSize.Width; column++)
            {
                for (var row = 0; row < gridSize.Height; row++)
                {
                    expectedLocations.Add(new Location((byte)column, (byte)row));
                }
            }

            var sut = new AiPlayer(fleetBuilderMock.Object);
            sut.GetFleet(gridSize);

            var actualLocations = new List<Location>();
            for (var i = 0; i < gridSize.Width * gridSize.Height; i++)
            {
                actualLocations.Add(sut.GetShot(oceanGridMock.Object, targetGridMock.Object));
            }

            Assert.True(!expectedLocations.Except(actualLocations).Any() && expectedLocations.Count == actualLocations.Count);
        }
    }
}
