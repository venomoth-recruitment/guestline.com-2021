using System.Collections.Generic;
using Moq;
using Xunit;
using Game.Core.Consoles;
using Game.Core.Fleets;
using Game.Core.Grids;
using Game.Core.Players;

namespace Game.Core.Tests.Players
{
    public class ConsolePlayerTests
    {
        private string _output = "";

        [Fact]
        public void GetFleet_ShouldReturnFleetBuiltByFleetBuilder()
        {
            var gridSize = new GridSize(4, 4);
            var theme = this.GetTheme();

            var consoleMock = new Mock<IConsole>();
            var fleetMock = new Mock<IFleet>();

            var fleetBuilderMock = new Mock<IFleetBuilder>();
            fleetBuilderMock.Setup(fleetBuilder => fleetBuilder.Build(gridSize)).Returns(fleetMock.Object);

            var sut = new ConsolePlayer(consoleMock.Object, theme, fleetBuilderMock.Object);

            Assert.Same(fleetMock.Object, sut.GetFleet(gridSize));
        }

        [Fact]
        public void GetShot_ShouldDrawGridsPrintTextAndReturnValueFromConsole()
        {
            var expectedShot = new Location(1, 1);

            var gridSize = new GridSize(3, 3);
            var theme = this.GetTheme();

            var consoleMock = new Mock<IConsole>();
            consoleMock.Setup(console => console.ReadLine()).Returns("B2");

            var fleetBuilderMock = new Mock<IFleetBuilder>();

            var oceanGridMock = new Mock<IGrid>();
            oceanGridMock.Setup(grid => grid.Size).Returns(gridSize);
            oceanGridMock.Setup(grid => grid.GetLocations()).Returns(new Dictionary<Location, GridLocationState> {
                { new Location(0, 0), GridLocationState.Free },
                { new Location(0, 1), GridLocationState.Free },
                { new Location(0, 2), GridLocationState.Free },
                { new Location(1, 0), GridLocationState.Ship },
                { new Location(1, 1), GridLocationState.Ship },
                { new Location(1, 2), GridLocationState.Free },
                { new Location(2, 0), GridLocationState.Free },
                { new Location(2, 1), GridLocationState.Free },
                { new Location(2, 2), GridLocationState.Free }
            });

            var targetGridMock = new Mock<IGrid>();
            targetGridMock.Setup(grid => grid.Size).Returns(gridSize);
            targetGridMock.Setup(grid => grid.GetLocations()).Returns(new Dictionary<Location, GridLocationState> {
                { new Location(0, 0), GridLocationState.Free },
                { new Location(0, 1), GridLocationState.Free },
                { new Location(0, 2), GridLocationState.Free },
                { new Location(1, 0), GridLocationState.Free },
                { new Location(1, 1), GridLocationState.Free },
                { new Location(1, 2), GridLocationState.Free },
                { new Location(2, 0), GridLocationState.Free },
                { new Location(2, 1), GridLocationState.Free },
                { new Location(2, 2), GridLocationState.Free }
            });

            var sut = new ConsolePlayer(consoleMock.Object, theme, fleetBuilderMock.Object);
            var actualShot = sut.GetShot(oceanGridMock.Object, targetGridMock.Object);

            consoleMock.Verify(console => console.Write(It.Is<string>(str => this.AppendOutput(str))), Times.AtMost(1000));
            consoleMock.Verify(console => console.WriteLine(It.Is<string>(str => this.AppendOutput(str + "\n"))), Times.AtMost(1000));

            Assert.Equal(expectedShot, actualShot);

            Assert.Equal("     A   B   C   \n   +---+---+---+\n1  |   |   |   |   +---+---+---+2  |   |   |   |   +---+---+---+"
                + "3  |   |   |   |   +---+---+---+     A   B   C   \n   +---+---+---+\n1  |   | o |   |   +---+---+---+"
                + "2  |   | o |   |   +---+---+---+3  |   |   |   |   +---+---+---+It's your turn now, give a shot: "
                + "\r\n\r\n\r\n\r\n\r\n\r\n\n\r\n\r\n\r\n\r\n\r\n\r\n\n", this._output);
        }

        // TODO Write more tests

        private ConsoleTheme GetTheme()
        {
            return new ConsoleTheme(
                3,
                ' ',
                'x',
                '*',
                'o',
                'x',
                ConsoleColor.White,
                ConsoleColor.Blue,
                ConsoleColor.White,
                ConsoleColor.White,
                ConsoleColor.Yellow,
                ConsoleColor.Cyan,
                ConsoleColor.Green,
                ConsoleColor.Red
            );
        }

        private bool AppendOutput(string str)
        {
            this._output = this._output + str;

            return true;
        }
    }
}
