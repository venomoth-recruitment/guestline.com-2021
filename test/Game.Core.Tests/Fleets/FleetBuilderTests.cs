using System;
using Xunit;
using Game.Core.Fleets;
using Game.Core.Grids;

namespace Game.Core.Tests.Fleets
{
    public class FleetBuilderTests
    {
        [Fact]
        public void Build_ShouldReturnFleetOfGivenShipsSpecification()
        {
            var shipOneName = "Ship1";
            var shipTwoName = "ShipOne";
            var shipThreeName = "ShipHundred";
            var shipFourName = "ShipTwo55";

            byte shipOneSize = 1;
            byte shipTwoSize = 8;
            byte shipThreeSize = 100;
            byte shipFourSize = 255;

            var gridSize = new GridSize(255, 255);

            var sut = new FleetBuilder();
            sut.AddShip(shipOneName, shipOneSize);
            sut.AddShip(shipTwoName, shipTwoSize);
            sut.AddShip(shipThreeName, shipThreeSize);
            sut.AddShip(shipFourName, shipFourSize);

            var fleet = sut.Build(gridSize);
            var ships = fleet.GetShips();

            Assert.Equal(4, ships.Count);
            Assert.Equal(shipOneName, ships[0].Name);
            Assert.Equal(shipTwoName, ships[1].Name);
            Assert.Equal(shipThreeName, ships[2].Name);
            Assert.Equal(shipFourName, ships[3].Name);
            Assert.Equal(shipOneSize, ships[0].GetLocations().Count);
            Assert.Equal(shipTwoSize, ships[1].GetLocations().Count);
            Assert.Equal(shipThreeSize, ships[2].GetLocations().Count);
            Assert.Equal(shipFourSize, ships[3].GetLocations().Count);
        }

        [Fact]
        public void Build_ShouldThrowExceptionWhenShipIsBiggerThanGrid()
        {
            var gridSize = new GridSize(10, 10);

            var sut = new FleetBuilder();
            sut.AddShip("ShipName", 11);

            var exception = Assert.Throws<Exception>(() => sut.Build(gridSize));
            Assert.Equal("Could not find any available location for the ship on the grid. Is grid too small for the ship?", exception.Message);
        }

        [Fact]
        public void Build_ShouldThrowExceptionWhenCantPlaceAllShipsOnGrid()
        {
            var gridSize = new GridSize(4, 4);

            var sut = new FleetBuilder();
            sut.AddShip("ShipName", 4);
            sut.AddShip("ShipName", 4);
            sut.AddShip("ShipName", 4);
            sut.AddShip("ShipName", 4);
            sut.AddShip("ShipName", 4);

            var exception = Assert.Throws<Exception>(() => sut.Build(gridSize));
            Assert.Equal("Could not build fleet. Try again or try to chage ships size.", exception.Message);
        }

        [Fact]
        public void Build_ShouldThrowExceptionWhenShipSizeIsZero()
        {
            var gridSize = new GridSize(1, 1);

            var sut = new FleetBuilder();
            sut.AddShip("ShipName", 0);

            var exception = Assert.Throws<ArgumentException>(() => sut.Build(gridSize));
            Assert.Equal($"Size of the ship is invalid. It must be between 1 and {byte.MaxValue}.", exception.Message);
        }
    }
}
