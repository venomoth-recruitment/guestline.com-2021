using System.Collections.Generic;
using Moq;
using Xunit;
using Game.Core.Fleets;
using Game.Core.Ships;

namespace Game.Core.Test.Fleets
{
    public class FleetTests
    {
        [Fact]
        public void IsSunk_ShouldReturnTrueWhenAllShipsAreSunk()
        {
            var shipMockOne = new Mock<IShip>();
            shipMockOne.Setup(ship => ship.IsSunk).Returns(true);
            shipMockOne.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location>());

            var shipMockTwo = new Mock<IShip>();
            shipMockTwo.Setup(ship => ship.IsSunk).Returns(true);
            shipMockTwo.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location>());

            var shipMockThree = new Mock<IShip>();
            shipMockThree.Setup(ship => ship.IsSunk).Returns(true);
            shipMockThree.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location>());

            var sut = new Fleet();
            sut.TryAddShip(shipMockOne.Object);
            sut.TryAddShip(shipMockTwo.Object);
            sut.TryAddShip(shipMockThree.Object);

            Assert.True(sut.IsSunk);
        }

        [Fact]
        public void IsSunk_ShouldReturnFalseWhenNotAllShipsAreSunk()
        {
            var shipMockOne = new Mock<IShip>();
            shipMockOne.Setup(ship => ship.IsSunk).Returns(false);
            shipMockOne.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location>());

            var shipMockTwo = new Mock<IShip>();
            shipMockTwo.Setup(ship => ship.IsSunk).Returns(true);
            shipMockTwo.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location>());

            var shipMockThree = new Mock<IShip>();
            shipMockThree.Setup(ship => ship.IsSunk).Returns(true);
            shipMockThree.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location>());

            var sut = new Fleet();
            sut.TryAddShip(shipMockOne.Object);
            sut.TryAddShip(shipMockTwo.Object);
            sut.TryAddShip(shipMockThree.Object);

            Assert.False(sut.IsSunk);
        }

        [Fact]
        public void GetShip_ShouldReturnProperShip()
        {
            var shipMockOne = new Mock<IShip>();
            shipMockOne.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(0, 0), new Location(0, 1) });

            var shipMockTwo = new Mock<IShip>();
            shipMockTwo.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(1, 0), new Location(1, 1) });

            var shipMockThree = new Mock<IShip>();
            shipMockThree.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(2, 0), new Location(2, 1) });

            var sut = new Fleet();
            sut.TryAddShip(shipMockOne.Object);
            sut.TryAddShip(shipMockTwo.Object);
            sut.TryAddShip(shipMockThree.Object);

            Assert.Same(shipMockOne.Object, sut.GetShip(new Location(0, 1)));
            Assert.Same(shipMockTwo.Object, sut.GetShip(new Location(1, 1)));
            Assert.Same(shipMockThree.Object, sut.GetShip(new Location(2, 1)));
        }

        [Fact]
        public void GetShips_ShouldReturnAllAddedShips()
        {
            var shipMockOne = new Mock<IShip>();
            shipMockOne.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(0, 0), new Location(0, 1) });

            var shipMockTwo = new Mock<IShip>();
            shipMockTwo.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(1, 0), new Location(1, 1) });

            var shipMockThree = new Mock<IShip>();
            shipMockThree.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(2, 0), new Location(2, 1) });

            var sut = new Fleet();
            sut.TryAddShip(shipMockOne.Object);
            sut.TryAddShip(shipMockTwo.Object);
            sut.TryAddShip(shipMockThree.Object);

            Assert.Equal(new List<IShip> { shipMockOne.Object, shipMockTwo.Object, shipMockThree.Object }, sut.GetShips());
        }

        [Fact]
        public void TryAddShip_ShouldReturnTrueWhenThereIsNoCollision()
        {
            var shipMockOne = new Mock<IShip>();
            shipMockOne.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(0, 0), new Location(0, 1) });

            var shipMockTwo = new Mock<IShip>();
            shipMockTwo.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(1, 0), new Location(1, 1) });

            var sut = new Fleet();
            sut.TryAddShip(shipMockOne.Object);

            Assert.True(sut.TryAddShip(shipMockTwo.Object));
        }

        [Fact]
        public void TryAddShip_ShouldReturnFalseWhenThereIsNoCollision()
        {
            var shipMockOne = new Mock<IShip>();
            shipMockOne.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(0, 0), new Location(0, 1) });

            var shipMockTwo = new Mock<IShip>();
            shipMockTwo.Setup(ship => ship.GetLocations()).Returns(new HashSet<Location> { new Location(0, 1), new Location(1, 1) });

            var sut = new Fleet();
            sut.TryAddShip(shipMockOne.Object);

            Assert.False(sut.TryAddShip(shipMockTwo.Object));
        }
    }
}
