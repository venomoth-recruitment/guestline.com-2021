using System;
using System.Collections.Generic;
using Xunit;
using Game.Core.Ships;

namespace Game.Core.Tests.Ships
{
    public class ShipTests
    {
        [Fact]
        public void Constructor_ShouldThrowExceptionWhenLocationIsEmpty()
        {
            var exception = Assert.Throws<ArgumentException>(() => new Ship("shipName", new HashSet<Location>()));
            Assert.Equal($"Size of the ship is invalid. It must be between 1 and {byte.MaxValue}.", exception.Message);
        }

        [Fact]
        public void Constructor_ShouldThrowExceptionWhenAnyLocationIsNull()
        {
            var locations = new HashSet<Location> { new Location(0, 0), new Location(0, 1), null };

            var exception = Assert.Throws<ArgumentException>(() => new Ship("shipName", locations));
            Assert.Equal("Location of the ship is invalid.", exception.Message);
        }

        [Fact]
        public void Constructor_ShouldThrowExceptionWhenShipIsNotHorizontalOrVertical()
        {
            var locations = new HashSet<Location> { new Location(0, 0), new Location(0, 1), new Location(1, 1) };

            var exception = Assert.Throws<ArgumentException>(() => new Ship("shipName", locations));
            Assert.Equal("Location of the ship is invalid.", exception.Message);
        }

        [Fact]
        public void IsSunk_ShouldReturnTrueWhenAllLocationsAreHit()
        {
            var locationOne = new Location(0, 0);
            var locationTwo = new Location(0, 1);
            var locationThree = new Location(0, 2);

            var sut = new Ship("shipName", new HashSet<Location> { locationOne, locationTwo, locationThree });
            sut.TryHit(locationOne);
            sut.TryHit(locationTwo);
            sut.TryHit(locationThree);

            Assert.True(sut.IsSunk);
        }

        [Fact]
        public void IsSunk_ShouldReturnFalseWhenNotAllLocationsAreHit()
        {
            var locationOne = new Location(0, 0);
            var locationTwo = new Location(1, 0);
            var locationThree = new Location(2, 0);

            var sut = new Ship("shipName", new HashSet<Location> { locationOne, locationTwo, locationThree });
            sut.TryHit(locationOne);
            sut.TryHit(locationThree);

            Assert.False(sut.IsSunk);
        }

        [Fact]
        public void GetLocations_ShouldReturnAllLocations()
        {
            var locations = new HashSet<Location> { new Location(0, 0), new Location(1, 0), new Location(2, 0) };

            var sut = new Ship("shipName", locations);

            Assert.Equal(locations, sut.GetLocations());
        }

        [Fact]
        public void GetLocations_ShouldReturnCopy()
        {
            var locations = new HashSet<Location> { new Location(0, 0), new Location(1, 0), new Location(2, 0) };

            var sut = new Ship("shipName", locations);
            var changedLocations = sut.GetLocations();
            changedLocations.Add(new Location(1, 1));

            Assert.NotEqual(changedLocations, sut.GetLocations());
        }

        [Fact]
        public void TryHit_ShouldReturnTrueWhenHitIsSuccessful()
        {
            var locationOne = new Location(0, 0);
            var locationTwo = new Location(1, 0);

            var sut = new Ship("shipName", new HashSet<Location> { locationOne, locationTwo });

            Assert.True(sut.TryHit(locationOne));
        }

        [Fact]
        public void TryHit_ShouldReturnFalseWhenHitIsMissed()
        {
            var locationOne = new Location(0, 0);
            var locationTwo = new Location(1, 0);
            var missedShotLocation = new Location(0, 1);

            var sut = new Ship("shipName", new HashSet<Location> { locationOne, locationTwo });

            Assert.False(sut.TryHit(missedShotLocation));
        }
    }
}
