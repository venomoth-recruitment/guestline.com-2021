﻿using System;
using Game.Core;
using GameEngine = Game.Core.Game;
using Game.Core.Consoles;
using ConsoleColor = Game.Core.Consoles.ConsoleColor;
using Game.Core.Fleets;
using Game.Core.Grids;
using Game.Core.Players;

namespace Game.Console.Single
{
    class Program
    {
        static void Main(string[] args)
        {
            var console = new SystemConsole();
            var theme = new ConsoleTheme(
                3,
                ' ',
                'x',
                '*',
                'o',
                'x',
                ConsoleColor.White,
                ConsoleColor.Blue,
                ConsoleColor.White,
                ConsoleColor.White,
                ConsoleColor.Yellow,
                ConsoleColor.Cyan,
                ConsoleColor.Green,
                ConsoleColor.Red
            );
            var gridSize = new GridSize(10, 10);

            var fleetBuilder = new FleetBuilder();
            fleetBuilder.AddShip("Destroyer", 4);
            fleetBuilder.AddShip("Destroyer", 4);
            fleetBuilder.AddShip("Batlleship", 5);

            var playerOne = new ConsolePlayer(console, theme, fleetBuilder);
            var playerTwo = new AiPlayer(fleetBuilder);

            var game = new GameEngine(gridSize, playerOne, playerTwo);
            game.Start();
        }
    }
}
