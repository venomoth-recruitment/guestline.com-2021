namespace Game.Core.Grids
{
    public class GridSize
    {
        private byte _height;
        private byte _width;

        public byte Height => _height;
        public byte Width => _width;

        public GridSize(byte height, byte width)
        {
            this._height = height;
            this._width = width;
        }
    }
}
