namespace Game.Core.Grids
{
    public enum GridLocationState
    {
        Free,
        Hit,
        Miss,
        Ship,
        Sunk
    }
}
