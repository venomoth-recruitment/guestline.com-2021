using System;
using System.Collections.Generic;

namespace Game.Core.Grids
{
    public class Grid : IGrid
    {
        private GridSize _size;
        private Dictionary<Location, GridLocationState> _locations = new Dictionary<Location, GridLocationState>();

        public GridSize Size => this._size;

        public Grid(GridSize size)
        {
            this._size = size;

            this.InitializeLocations();
        }

        private Grid(GridSize size, Dictionary<Location, GridLocationState> locations)
        {
            this._size = size;
            this._locations = locations;
        }

        private void InitializeLocations()
        {
            for (var column = 0; column < this.Size.Width; column++)
            {
                for (var row = 0; row < this.Size.Height; row++)
                {
                    this._locations.Add(new Location((byte)column, (byte)row), GridLocationState.Free);
                }
            }
        }

        public Dictionary<Location, GridLocationState> GetLocations()
        {
            return new Dictionary<Location, GridLocationState>(this._locations);
        }

        public IGrid SetLocationState(Location location, GridLocationState state)
        {
            if (!this._locations.ContainsKey(location))
            {
                this.ThrowLocationNotExistException();
            }

            var locationsCopy = new Dictionary<Location, GridLocationState>(this._locations);
            locationsCopy[location] = state;

            return new Grid(this._size, locationsCopy);
        }

        public IGrid SetLocationsState(Dictionary<Location, GridLocationState> locationsState)
        {
            var locationsCopy = new Dictionary<Location, GridLocationState>(this._locations);

            foreach (var locationState in locationsState)
            {
                if (!this._locations.ContainsKey(locationState.Key))
                {
                    this.ThrowLocationNotExistException();
                }

                locationsCopy[locationState.Key] = locationState.Value;
            }

            return new Grid(this._size, locationsCopy);
        }

        private void ThrowLocationNotExistException()
        {
            throw new ArgumentException("Location does not exist.");
        }
    }
}
