using System.Collections.Generic;

namespace Game.Core.Grids
{
    public interface IGrid
    {
        GridSize Size
        {
            get;
        }

        Dictionary<Location, GridLocationState> GetLocations();

        IGrid SetLocationState(Location location, GridLocationState state);

        IGrid SetLocationsState(Dictionary<Location, GridLocationState> locationsState);
    }
}
