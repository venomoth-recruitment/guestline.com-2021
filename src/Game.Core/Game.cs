using System;
using System.Collections.Generic;
using Game.Core.Fleets;
using Game.Core.Grids;
using Game.Core.Players;
using Game.Core.Ships;

namespace Game.Core
{
    public class Game
    {
        private GridSize _gridSize;
        private PlayerBoard _playerOneBoard;
        private PlayerBoard _playerTwoBoard;
        private GameState _state = GameState.Started;

        public Game(GridSize gridSize, IPlayer playerOne, IPlayer playerTwo)
        {
            this._gridSize = gridSize;
            this._playerOneBoard = new PlayerBoard(playerOne, new Grid(gridSize), new Grid(gridSize));
            this._playerTwoBoard = new PlayerBoard(playerTwo, new Grid(gridSize), new Grid(gridSize));
        }

        public void Start()
        {
            var exitGame = false;

            while (!exitGame)
            {
                switch (this._state)
                {
                    case GameState.Started:
                        this._playerOneBoard.Fleet = this._playerOneBoard.Player.GetFleet(this._gridSize);
                        this._playerTwoBoard.Fleet = this._playerTwoBoard.Player.GetFleet(this._gridSize);

                        this._playerOneBoard.OceanGrid = this.MarkFleetOnGrid(this._playerOneBoard.Fleet, this._playerOneBoard.OceanGrid);
                        this._playerTwoBoard.OceanGrid = this.MarkFleetOnGrid(this._playerTwoBoard.Fleet, this._playerTwoBoard.OceanGrid);

                        this._state = GameState.FleetsPlaced;
                        break;
                    case GameState.FleetsPlaced:
                    case GameState.PlayerOneTurn:
                        this.HandleShot(this._playerOneBoard, this._playerTwoBoard);
                        this._state = this._playerTwoBoard.Fleet.IsSunk ? GameState.PlayerOneWon : GameState.PlayerTwoTurn;
                        break;
                    case GameState.PlayerTwoTurn:
                        this.HandleShot(this._playerTwoBoard, this._playerOneBoard);
                        this._state = this._playerOneBoard.Fleet.IsSunk ? GameState.PlayerTwoWon : GameState.PlayerOneTurn;
                        break;
                    case GameState.PlayerOneWon:
                        this._playerTwoBoard.Player.ShowDefeat();
                        this._playerOneBoard.Player.ShowVictory();
                        this._state = GameState.Finished;
                        break;
                    case GameState.PlayerTwoWon:
                        this._playerOneBoard.Player.ShowDefeat();
                        this._playerTwoBoard.Player.ShowVictory();
                        this._state = GameState.Finished;
                        break;
                    case GameState.Finished:
                        exitGame = true;
                        break;
                    default:
                        throw new Exception("Unknown game state.");
                }
            }
        }

        private void HandleShot(PlayerBoard shootingPlayerBoard, PlayerBoard firedPlayerBoard)
        {
            firedPlayerBoard.Player.ShowWaitForOpponentShot(firedPlayerBoard.OceanGrid, firedPlayerBoard.TargetGrid);

            var shotLocation = shootingPlayerBoard.Player.GetShot(shootingPlayerBoard.OceanGrid, shootingPlayerBoard.TargetGrid);
            var shipAtShotLocation = firedPlayerBoard.Fleet.GetShip(shotLocation);

            if (shipAtShotLocation != null && shipAtShotLocation.TryHit(shotLocation))
            {
                if (shipAtShotLocation.IsSunk)
                {
                    this.HandleSunkShot(shipAtShotLocation, shootingPlayerBoard, firedPlayerBoard);
                    return;
                }

                this.HandleHitShot(shotLocation, shipAtShotLocation.Name, shootingPlayerBoard, firedPlayerBoard);
                return;
            }

            this.HandleMissedShot(shotLocation, shootingPlayerBoard, firedPlayerBoard);
        }

        private void HandleSunkShot(IShip hitShip, PlayerBoard shootingPlayerBoard, PlayerBoard firedPlayerBoard)
        {
            shootingPlayerBoard.TargetGrid = this.SetGridStateAtShipLocations(
                hitShip, shootingPlayerBoard.TargetGrid, GridLocationState.Sunk);
            firedPlayerBoard.OceanGrid = this.SetGridStateAtShipLocations(
                hitShip, firedPlayerBoard.OceanGrid, GridLocationState.Sunk);

            shootingPlayerBoard.Player.ShowShipSunk(shootingPlayerBoard.OceanGrid, shootingPlayerBoard.TargetGrid, hitShip.Name);
            firedPlayerBoard.Player.ShowOpponentShipSunk(firedPlayerBoard.OceanGrid, firedPlayerBoard.TargetGrid, hitShip.Name);
        }

        private void HandleHitShot(Location shotLocation, string hitShipName, PlayerBoard shootingPlayerBoard, PlayerBoard firedPlayerBoard)
        {
            shootingPlayerBoard.TargetGrid = shootingPlayerBoard.TargetGrid.SetLocationState(shotLocation, GridLocationState.Hit);
            firedPlayerBoard.OceanGrid = firedPlayerBoard.OceanGrid.SetLocationState(shotLocation, GridLocationState.Hit);

            shootingPlayerBoard.Player.ShowHitShot(shootingPlayerBoard.OceanGrid, shootingPlayerBoard.TargetGrid, hitShipName);
            firedPlayerBoard.Player.ShowOpponentHitShot(firedPlayerBoard.OceanGrid, firedPlayerBoard.TargetGrid, hitShipName);
        }

        private void HandleMissedShot(Location shotLocation, PlayerBoard shootingPlayerBoard, PlayerBoard firedPlayerBoard)
        {
            shootingPlayerBoard.TargetGrid = shootingPlayerBoard.TargetGrid.SetLocationState(shotLocation, GridLocationState.Miss);
            firedPlayerBoard.OceanGrid = firedPlayerBoard.OceanGrid.SetLocationState(shotLocation, GridLocationState.Miss);

            shootingPlayerBoard.Player.ShowMissedShot(shootingPlayerBoard.OceanGrid, shootingPlayerBoard.TargetGrid);
            firedPlayerBoard.Player.ShowOpponentMissedShot(firedPlayerBoard.OceanGrid, firedPlayerBoard.TargetGrid);
        }

        private IGrid MarkFleetOnGrid(IFleet fleet, IGrid grid)
        {
            var locationsState = new Dictionary<Location, GridLocationState>();

            foreach (var ship in fleet.GetShips())
            {
                foreach (var location in ship.GetLocations())
                {
                    locationsState.Add(location, GridLocationState.Ship);
                }
            }

            return grid.SetLocationsState(locationsState);
        }

        private IGrid SetGridStateAtShipLocations(IShip ship, IGrid grid, GridLocationState state)
        {
            var locationsState = new Dictionary<Location, GridLocationState>();

            foreach (var location in ship.GetLocations())
            {
                locationsState.Add(location, state);
            }

            return grid.SetLocationsState(locationsState);
        }
    }
}
