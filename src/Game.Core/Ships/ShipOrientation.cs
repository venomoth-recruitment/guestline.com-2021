namespace Game.Core.Ships
{
    public enum ShipOrientation
    {
        Horizontal,
        Vertical
    }
}
