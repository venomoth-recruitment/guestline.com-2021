using System.Collections.Generic;

namespace Game.Core.Ships
{
    public interface IShip
    {
        string Name
        { get; }

        bool IsSunk
        { get; }

        HashSet<Location> GetLocations();

        bool TryHit(Location location);
    }
}
