using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Ships
{
    public class Ship : IShip
    {
        private string _name;
        private HashSet<Location> _locations;
        private Dictionary<Location, bool> _hits;

        public string Name => this._name;

        public bool IsSunk => !this._hits.Where(hit => !hit.Value).Any();

        public Ship(string name, HashSet<Location> locations)
        {
            if (!this.isSizeValid(locations))
            {
                throw new ArgumentException($"Size of the ship is invalid. It must be between 1 and {byte.MaxValue}.");
            }

            if (!this.isLocationValid(locations))
            {
                throw new ArgumentException("Location of the ship is invalid.");
            }

            this._name = name;
            this._locations = locations;
            this._hits = locations.ToDictionary(location => location, location => false);
        }

        private bool isSizeValid(HashSet<Location> locations)
        {
            return locations.Count > 0 && locations.Count <= byte.MaxValue;
        }

        private bool isLocationValid(HashSet<Location> locations)
        {
            if (locations.Count == 1)
            {
                return true;
            }

            if (this.LocationsContainNulls(locations))
            {
                return false;
            }

            if (!this.IsHorizontalOrVertical(locations))
            {
                return false;
            }

            return true;
        }

        private bool LocationsContainNulls(HashSet<Location> locations)
        {
            return locations.Where(loc => loc == null).Any();
        }

        private bool IsHorizontalOrVertical(HashSet<Location> locations)
        {
            var orderedLocations = locations.OrderBy(
                location => $"{location.Column.ToString().PadLeft(3, '0')}{location.Row.ToString().PadLeft(3, '0')}").ToArray();

            var isHorizontal = true;
            var isVertical = true;

            for (var currentIndex = 1; currentIndex < orderedLocations.Length; currentIndex++)
            {
                var previousIndex = currentIndex - 1;

                if (orderedLocations[currentIndex].Column != orderedLocations[previousIndex].Column + 1)
                {
                    isHorizontal = false;
                }

                if (orderedLocations[currentIndex].Row != orderedLocations[previousIndex].Row + 1)
                {
                    isVertical = false;
                }

                if (!isHorizontal && !isVertical)
                {
                    return false;
                }
            }

            return true;
        }

        public HashSet<Location> GetLocations() => new HashSet<Location>(this._locations);

        public bool TryHit(Location location)
        {
            var foundLocation = this._locations.SingleOrDefault(loc => loc.Equals(location));

            if (foundLocation != null)
            {
                this._hits[location] = true;

                return true;
            }

            return false;
        }
    }
}
