﻿using Game.Core.Fleets;
using Game.Core.Grids;

namespace Game.Core.Players
{
    public interface IPlayer
    {
        IFleet GetFleet(GridSize gridSize);

        Location GetShot(IGrid oceanGrid, IGrid targetGrid);

        void ShowWaitForOpponentShot(IGrid oceanGrid, IGrid targetGrid);

        void ShowHitShot(IGrid oceanGrid, IGrid targetGrid, string shipName);

        void ShowShipSunk(IGrid oceanGrid, IGrid targetGrid, string shipName);

        void ShowMissedShot(IGrid oceanGrid, IGrid targetGrid);

        void ShowOpponentHitShot(IGrid oceanGrid, IGrid targetGrid, string shipName);

        void ShowOpponentMissedShot(IGrid oceanGrid, IGrid targetGrid);

        void ShowOpponentShipSunk(IGrid oceanGrid, IGrid targetGrid, string shipName);

        void ShowDefeat();

        void ShowVictory();
    }
}
