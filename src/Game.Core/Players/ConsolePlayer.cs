using System;
using System.Text.RegularExpressions;
using Game.Core.Consoles;
using ConsoleColor = Game.Core.Consoles.ConsoleColor;
using Game.Core.Fleets;
using Game.Core.Grids;

namespace Game.Core.Players
{
    public class ConsolePlayer : IPlayer
    {
        private IConsole _console;
        private ConsoleTheme _theme;
        private IFleetBuilder _fleetBuilder;
        private Regex _shotRegex;

        public ConsolePlayer(IConsole console, ConsoleTheme theme, IFleetBuilder fleetBuilder)
        {
            this._console = console;
            this._theme = theme;
            this._fleetBuilder = fleetBuilder;
            this.SetRegex();
        }

        public IFleet GetFleet(GridSize gridSize)
        {
            return this._fleetBuilder.Build(gridSize);
        }

        public Location GetShot(IGrid oceanGrid, IGrid targetGrid)
        {
            this.DrawGrids(oceanGrid, targetGrid);
            this.WriteText("It's your turn now, give a shot: ");

            while (true)
            {
                var shotLocation = this._console.ReadLine();

                if (!this._shotRegex.IsMatch(shotLocation))
                {
                    this.WriteText("Wrong value, try again: ");
                    continue;
                }

                var columnAndRow = this._shotRegex.Split(shotLocation);
                var columnChar = columnAndRow[1].ToCharArray()[0];
                var columnNum = columnChar % 32;
                var rowNum = byte.Parse(columnAndRow[2]);

                if (columnNum > targetGrid.Size.Width || rowNum > targetGrid.Size.Height)
                {
                    this.WriteText("Wow! You're trying to shot out of the battle field, try again: ");
                    continue;
                }

                return new Location((byte)(columnNum - 1), (byte)(rowNum - 1));
            }
        }

        public void ShowWaitForOpponentShot(IGrid oceanGrid, IGrid targetGrid)
        {
            this.DrawGrids(oceanGrid, targetGrid);
            this.WriteText("Your opponent is trying to hit you. Wait and think about your next shot.");
        }

        public void ShowHitShot(IGrid oceanGrid, IGrid targetGrid, string shipName)
        {
            this.DrawGrids(oceanGrid, targetGrid);
            this.WriteText($"You've hit! It's {shipName}.");
            this.ShowContinueMessage();
        }

        public void ShowShipSunk(IGrid oceanGrid, IGrid targetGrid, string shipName)
        {
            this.DrawGrids(oceanGrid, targetGrid);
            this.WriteText($"Great shot! Your enemy has lost {shipName}.");
            this.ShowContinueMessage();
        }

        public void ShowMissedShot(IGrid oceanGrid, IGrid targetGrid)
        {
            this.DrawGrids(oceanGrid, targetGrid);
            this.WriteText("Your shot hit some fish. Better luck next time.");
            this.ShowContinueMessage();
        }

        public void ShowOpponentHitShot(IGrid oceanGrid, IGrid targetGrid, string shipName)
        {
            this.DrawGrids(oceanGrid, targetGrid);
            this.WriteText($"Ooops, your {shipName} has been hit.");
            this.ShowContinueMessage();
        }

        public void ShowOpponentMissedShot(IGrid oceanGrid, IGrid targetGrid)
        {
            this.DrawGrids(oceanGrid, targetGrid);
            this.WriteText("Whew! The shot missed your ships.");
            this.ShowContinueMessage();
        }

        public void ShowOpponentShipSunk(IGrid oceanGrid, IGrid targetGrid, string shipName)
        {
            this.DrawGrids(oceanGrid, targetGrid);
            this.WriteText($"Oh no! Salute to your {shipName} and thank them for fantastic service.");
            this.ShowContinueMessage();
        }

        public void ShowDefeat()
        {
            this._console.Clear();
            this.WriteText("Unfortunately your army has been defeated. Maybe next time.");
        }

        public void ShowVictory()
        {
            this._console.Clear();
            this.WriteText("Congratulations! You've became king of the ocean.");
        }

        private void SetRegex()
        {
            this._shotRegex = new Regex(@"^([a-zA-Z])([1-9]|[1-2][0-9]|[3][0-2])$");
        }

        private void DrawGrids(IGrid oceanGrid, IGrid targetGrid)
        {
            this._console.Clear();
            this.DrawGrid(targetGrid);
            this._console.WriteLine("");
            this.DrawGrid(oceanGrid);
            this._console.WriteLine("");
        }

        private void DrawGrid(IGrid grid)
        {
            this.DrawColumnLabels(grid.Size.Width);
            this._console.Write("\n");
            this.DrawRowSeparator(grid.Size.Width);
            this._console.Write("\n");
            this.DrawRows(grid);
        }

        private void DrawColumnLabels(byte gridWidth)
        {
            this._console.ForegroundColor = this._theme.LabelColor;
            this._console.Write("  ");
            this._console.Write(new string(' ', this._theme.GridSpacingSize));

            for (var i = 0; i < gridWidth; i++)
            {
                this._console.Write(((char)(i + 65)).ToString());
                this._console.Write(new string(' ', this._theme.GridSpacingSize));
            }
        }

        private void DrawRows(IGrid grid)
        {
            var gridLocations = grid.GetLocations();

            for (var rowNum = 0; rowNum < grid.Size.Height; rowNum++)
            {
                this.DrawRowLabel(rowNum);

                this._console.ForegroundColor = this._theme.GridColor;
                this._console.Write("|");

                for (var columnNum = 0; columnNum < grid.Size.Width; columnNum++)
                {
                    var location = new Location((byte)columnNum, (byte)rowNum);
                    var locationState = gridLocations[location];

                    switch (locationState)
                    {
                        case GridLocationState.Free:
                            this.DrawLocation(this._theme.GridLocationStateFreeSymbol, this._theme.GridLocationStateFreeColor);
                            break;
                        case GridLocationState.Hit:
                            this.DrawLocation(this._theme.GridLocationStateHitSymbol, this._theme.GridLocationStateHitColor);
                            break;
                        case GridLocationState.Miss:
                            this.DrawLocation(this._theme.GridLocationStateMissSymbol, this._theme.GridLocationStateMissColor);
                            break;
                        case GridLocationState.Ship:
                            this.DrawLocation(this._theme.GridLocationStateShipSymbol, this._theme.GridLocationStateShipColor);
                            break;
                        case GridLocationState.Sunk:
                            this.DrawLocation(this._theme.GridLocationStateSunkSymbol, this._theme.GridLocationStateSunkColor);
                            break;
                        default:
                            throw new Exception("Unsupported grid location state.");
                    }

                    this._console.ForegroundColor = this._theme.GridColor;
                    this._console.Write("|");
                }

                this._console.WriteLine("\r");
                this.DrawRowSeparator(grid.Size.Width);
                this._console.WriteLine("\r");
            }
        }

        private void DrawRowLabel(int rowNum)
        {
            var rowLabel = (rowNum + 1).ToString().PadRight(this._theme.GridSpacingSize);
            this._console.ForegroundColor = this._theme.LabelColor;
            this._console.Write(rowLabel);
        }

        private void DrawRowSeparator(byte gridWidth)
        {
            this._console.ForegroundColor = this._theme.GridColor;
            this._console.Write(new string(' ', this._theme.GridSpacingSize));
            this._console.Write("+");

            for (var i = 0; i < gridWidth; i++)
            {
                this._console.Write(new string('-', this._theme.GridSpacingSize));
                this._console.Write("+");
            }
        }

        private void DrawLocation(char symbol, ConsoleColor color)
        {
            var padding = (int)((this._theme.GridSpacingSize - 1) / 2);
            var str = symbol.ToString().PadLeft(1 + padding).PadRight(1 + padding * 2);
            this._console.ForegroundColor = color;
            this._console.Write(str);
        }

        private void WriteText(string text)
        {
            this._console.ForegroundColor = this._theme.TextColor;
            this._console.Write(text);
        }

        private void ShowContinueMessage()
        {
            this._console.Write(" Press any key to continue...");
            this._console.ReadKey();
        }
    }
}
