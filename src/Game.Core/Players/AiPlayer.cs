using System;
using System.Collections.Generic;
using Game.Core.Fleets;
using Game.Core.Grids;

namespace Game.Core.Players
{
    public class AiPlayer : IPlayer
    {
        private Random _random = new Random();
        private IFleetBuilder _fleetBuilder;
        private List<Location> _unshootedLocations;

        public AiPlayer(IFleetBuilder fleetBuilder)
        {
            this._fleetBuilder = fleetBuilder;
        }

        public IFleet GetFleet(GridSize gridSize)
        {
            this.InitializeUnshootedLocations(gridSize);

            return this._fleetBuilder.Build(gridSize);
        }

        private void InitializeUnshootedLocations(GridSize gridSize)
        {
            this._unshootedLocations = new List<Location>();

            for (var column = 0; column < gridSize.Width; column++)
            {
                for (var row = 0; row < gridSize.Height; row++)
                {
                    this._unshootedLocations.Add(new Location((byte)column, (byte)row));
                }
            }
        }

        public Location GetShot(IGrid oceanGrid, IGrid targetGrid)
        {
            var randomIndex = this._random.Next(this._unshootedLocations.Count);
            var shot = this._unshootedLocations[randomIndex];
            this._unshootedLocations.RemoveAt(randomIndex);

            return shot;
        }

        public void ShowWaitForOpponentShot(IGrid oceanGrid, IGrid targetGrid)
        { }

        public void ShowHitShot(IGrid oceanGrid, IGrid targetGrid, string shipName)
        { }

        public void ShowShipSunk(IGrid oceanGrid, IGrid targetGrid, string shipName)
        { }

        public void ShowMissedShot(IGrid oceanGrid, IGrid targetGrid)
        { }

        public void ShowOpponentHitShot(IGrid oceanGrid, IGrid targetGrid, string shipName)
        { }

        public void ShowOpponentMissedShot(IGrid oceanGrid, IGrid targetGrid)
        { }

        public void ShowOpponentShipSunk(IGrid oceanGrid, IGrid targetGrid, string shipName)
        { }

        public void ShowDefeat()
        { }

        public void ShowVictory()
        { }
    }
}
