using System;

namespace Game.Core
{
    public class Location
    {
        private byte _column;
        private byte _row;

        public byte Column => this._column;
        public byte Row => this._row;
        public Location(byte column, byte row)
        {
            this._column = column;
            this._row = row;
        }

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Location location = (Location)obj;

                return (this.Column == location.Column) && (this.Row == location.Row);
            }
        }

        public override int GetHashCode()
        {
            return this.Column.GetHashCode() ^ this.Row.GetHashCode();
        }
    }
}
