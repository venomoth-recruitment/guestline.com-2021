using System.Collections.Generic;
using Game.Core.Ships;

namespace Game.Core.Fleets
{
    public interface IFleet
    {
        bool IsSunk
        {
            get;
        }

        IShip GetShip(Location location);

        List<IShip> GetShips();
    }
}
