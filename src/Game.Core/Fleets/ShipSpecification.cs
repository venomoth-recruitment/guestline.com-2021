namespace Game.Core.Fleets
{
    public class ShipSpecification
    {
        private string _name;
        private byte _size;
        public string Name => this._name;
        public byte Size => this._size;

        public ShipSpecification(string name, byte size)
        {
            this._name = name;
            this._size = size;
        }
    }
}
