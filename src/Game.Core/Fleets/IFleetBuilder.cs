using Game.Core.Grids;

namespace Game.Core.Fleets
{
    public interface IFleetBuilder
    {
        void AddShip(string name, byte size);

        IFleet Build(GridSize gridSize);
    }
}
