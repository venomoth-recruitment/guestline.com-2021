using System.Collections.Generic;
using Game.Core.Ships;

namespace Game.Core.Fleets
{
    public class Fleet : IFleet
    {
        private List<IShip> _ships = new List<IShip>();

        public bool IsSunk
        {
            get
            {
                foreach (var ship in this._ships)
                {
                    if (!ship.IsSunk)
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        public IShip GetShip(Location location)
        {
            foreach (var ship in this._ships)
            {
                foreach (var shipLocation in ship.GetLocations())
                {
                    if (location.Equals(shipLocation))
                    {
                        return ship;
                    }
                }
            }

            return null;
        }

        public List<IShip> GetShips()
        {
            return this._ships;
        }

        public bool TryAddShip(IShip ship)
        {
            if (this.hasCollisionWithOtherShips(ship))
            {
                return false;
            }

            this._ships.Add(ship);

            return true;
        }

        private bool hasCollisionWithOtherShips(IShip ship)
        {
            foreach (var otherShip in this._ships)
            {
                foreach (var otherShipLocation in otherShip.GetLocations())
                {
                    foreach (var shipLocation in ship.GetLocations())
                    {
                        if (otherShipLocation.Equals(shipLocation))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}
