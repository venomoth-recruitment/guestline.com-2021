using System;
using System.Collections.Generic;
using Game.Core.Grids;
using Game.Core.Ships;

namespace Game.Core.Fleets
{
    public class FleetBuilder : IFleetBuilder
    {
        private Random _random = new Random();
        private List<ShipSpecification> _shipSpecifications = new List<ShipSpecification>();

        public void AddShip(string name, byte size)
        {
            this._shipSpecifications.Add(new ShipSpecification(name, size));
        }

        public IFleet Build(GridSize gridSize)
        {
            var fleet = new Fleet();

            foreach (var shipSpecification in this._shipSpecifications)
            {
                var attemptsCount = 0;
                while (true)
                {
                    var ship = this.GenerateShip(shipSpecification.Name, shipSpecification.Size, gridSize);

                    if (fleet.TryAddShip(ship))
                    {
                        break;
                    }

                    if (++attemptsCount > 100)
                    {
                        throw new Exception("Could not build fleet. Try again or try to chage ships size.");
                    }
                }
            }

            return fleet;
        }

        private Ship GenerateShip(string shipName, byte shipSize, GridSize gridSize)
        {
            var shipOrientation = this.GenerateShipOrientation();

            switch (shipOrientation)
            {
                case ShipOrientation.Horizontal:
                    return this.GenerateHorizontalShip(shipName, shipSize, gridSize);
                case ShipOrientation.Vertical:
                    return this.GenerateVerticalShip(shipName, shipSize, gridSize);
                default:
                    throw new ArgumentOutOfRangeException("Unsupported ship orienation.");
            }
        }

        private ShipOrientation GenerateShipOrientation()
        {
            var shipOrientations = Enum.GetValues(typeof(ShipOrientation));
            var randomIndex = this._random.Next(shipOrientations.Length);

            return (ShipOrientation)shipOrientations.GetValue(randomIndex);
        }

        private Ship GenerateHorizontalShip(string shipName, byte shipSize, GridSize gridSize)
        {
            var availableStartLocations = this.GetAvailableStartLocationsForHorizontalShip(shipSize, gridSize);

            if (availableStartLocations.Count == 0)
            {
                this.ThrowLocationNotAvailableException();
            }

            var startLocation = this.GetRandomShipStartLocation(availableStartLocations);
            var locations = this.GetLocationsForHorizontalShip(shipSize, startLocation);

            return new Ship(shipName, locations);
        }

        private Ship GenerateVerticalShip(string shipName, byte shipSize, GridSize gridSize)
        {
            var availableStartLocations = this.GetAvailableStartLocationsForVerticalShip(shipSize, gridSize);

            if (availableStartLocations.Count == 0)
            {
                this.ThrowLocationNotAvailableException();
            }

            var startLocation = this.GetRandomShipStartLocation(availableStartLocations);
            var locations = this.GetLocationsForVerticalShip(shipSize, startLocation);

            return new Ship(shipName, locations);
        }

        private List<Location> GetAvailableStartLocationsForHorizontalShip(byte shipSize, GridSize gridSize)
        {
            var availableStartLocations = new List<Location>();

            var maxAvailableColumn = gridSize.Width - shipSize;

            for (byte column = 0; column <= maxAvailableColumn; column++)
            {
                for (byte row = 0; row < gridSize.Height; row++)
                {
                    availableStartLocations.Add(new Location(column, row));
                }
            }

            return availableStartLocations;
        }

        private List<Location> GetAvailableStartLocationsForVerticalShip(byte shipSize, GridSize gridSize)
        {
            var availableStartLocations = new List<Location>();

            var maxAvailableRow = gridSize.Height - shipSize;

            for (byte column = 0; column < gridSize.Width; column++)
            {
                for (byte row = 0; row <= maxAvailableRow; row++)
                {
                    availableStartLocations.Add(new Location(column, row));
                }
            }

            return availableStartLocations;
        }

        private void ThrowLocationNotAvailableException()
        {
            throw new Exception("Could not find any available location for the ship on the grid. Is grid too small for the ship?");
        }

        private Location GetRandomShipStartLocation(List<Location> availableLocations)
        {
            var randomIndex = this._random.Next(availableLocations.Count);

            return availableLocations[randomIndex];
        }

        private HashSet<Location> GetLocationsForHorizontalShip(byte shipSize, Location startLocation)
        {
            var locations = new HashSet<Location>();

            for (var i = 0; i < shipSize; i++)
            {
                locations.Add(new Location((byte)(startLocation.Column + i), startLocation.Row));
            }

            return locations;
        }

        private HashSet<Location> GetLocationsForVerticalShip(byte shipSize, Location startLocation)
        {
            var locations = new HashSet<Location>();

            for (var i = 0; i < shipSize; i++)
            {
                locations.Add(new Location(startLocation.Column, (byte)(startLocation.Row + i)));
            }

            return locations;
        }
    }
}
