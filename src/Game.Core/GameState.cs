namespace Game.Core
{
    public enum GameState
    {
        Started,
        FleetsPlaced,
        PlayerOneTurn,
        PlayerOneWon,
        PlayerTwoTurn,
        PlayerTwoWon,
        Finished
    }
}
