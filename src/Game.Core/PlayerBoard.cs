using Game.Core.Fleets;
using Game.Core.Grids;
using Game.Core.Players;

namespace Game.Core
{
    public class PlayerBoard
    {
        public IPlayer Player
        { get; }

        public IGrid OceanGrid
        { get; set; }

        public IGrid TargetGrid
        { get; set; }

        public IFleet Fleet
        { get; set; }

        public PlayerBoard(IPlayer player, IGrid oceanGrid = null, IGrid targetGrid = null, IFleet fleet = null)
        {
            this.Player = player;
            this.OceanGrid = oceanGrid;
            this.TargetGrid = targetGrid;
            this.Fleet = fleet;
        }
    }
}
