namespace Game.Core.Consoles
{
    public class ConsoleTheme
    {
        private int _gridSpacingSize;
        private char _gridLocationStateFreeSymbol;
        private char _gridLocationStateHitSymbol;
        private char _gridLocationStateMissSymbol;
        private char _gridLocationStateShipSymbol;
        private char _gridLocationStateSunkSymbol;
        private ConsoleColor _gridColor;
        private ConsoleColor _labelColor;
        private ConsoleColor _textColor;
        private ConsoleColor _gridLocationStateFreeColor;
        private ConsoleColor _gridLocationStateHitColor;
        private ConsoleColor _gridLocationStateMissColor;
        private ConsoleColor _gridLocationStateShipColor;
        private ConsoleColor _gridLocationStateSunkColor;

        public int GridSpacingSize => _gridSpacingSize;
        public char GridLocationStateFreeSymbol => _gridLocationStateFreeSymbol;
        public char GridLocationStateHitSymbol => _gridLocationStateHitSymbol;
        public char GridLocationStateMissSymbol => _gridLocationStateMissSymbol;
        public char GridLocationStateShipSymbol => _gridLocationStateShipSymbol;
        public char GridLocationStateSunkSymbol => _gridLocationStateSunkSymbol;
        public ConsoleColor GridColor => _gridColor;
        public ConsoleColor LabelColor => _labelColor;
        public ConsoleColor TextColor => _textColor;
        public ConsoleColor GridLocationStateFreeColor => _gridLocationStateFreeColor;
        public ConsoleColor GridLocationStateHitColor => _gridLocationStateHitColor;
        public ConsoleColor GridLocationStateMissColor => _gridLocationStateMissColor;
        public ConsoleColor GridLocationStateShipColor => _gridLocationStateShipColor;
        public ConsoleColor GridLocationStateSunkColor => _gridLocationStateSunkColor;

        public ConsoleTheme
        (
            int gridSpacingSize,
            char gridLocationStateFreeSymbol,
            char gridLocationStateHitSymbol,
            char gridLocationStateMissSymbol,
            char gridLocationStateShipSymbol,
            char gridLocationStateSunkSymbol,
            ConsoleColor gridColor,
            ConsoleColor labelColor,
            ConsoleColor textColor,
            ConsoleColor gridLocationStateFreeColor,
            ConsoleColor gridLocationStateHitColor,
            ConsoleColor gridLocationStateMissColor,
            ConsoleColor gridLocationStateShipColor,
            ConsoleColor gridLocationStateSunkColor
        )
        {
            this._gridSpacingSize = gridSpacingSize;
            this._gridLocationStateFreeSymbol = gridLocationStateFreeSymbol;
            this._gridLocationStateHitSymbol = gridLocationStateHitSymbol;
            this._gridLocationStateMissSymbol = gridLocationStateMissSymbol;
            this._gridLocationStateShipSymbol = gridLocationStateShipSymbol;
            this._gridLocationStateSunkSymbol = gridLocationStateSunkSymbol;
            this._gridColor = gridColor;
            this._labelColor = labelColor;
            this._textColor = textColor;
            this._gridLocationStateFreeColor = gridLocationStateFreeColor;
            this._gridLocationStateHitColor = gridLocationStateHitColor;
            this._gridLocationStateMissColor = gridLocationStateMissColor;
            this._gridLocationStateShipColor = gridLocationStateShipColor;
            this._gridLocationStateSunkColor = gridLocationStateSunkColor;
        }
    }
}
