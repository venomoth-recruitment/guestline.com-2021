using System;

namespace Game.Core.Consoles
{
    public class SystemConsole : IConsole
    {
        public ConsoleColor ForegroundColor
        {
            get => (ConsoleColor)Console.ForegroundColor;
            set
            {
                Console.ForegroundColor = (System.ConsoleColor)value;
            }
        }

        public void Clear()
        {
            Console.Clear();
        }

        public void Write(string str)
        {
            Console.Write(str);
        }

        public void ReadKey()
        {
            Console.ReadKey();
        }

        public string ReadLine()
        {
            return Console.ReadLine();
        }

        public void WriteLine(string line)
        {
            Console.WriteLine(line);
        }
    }
}
