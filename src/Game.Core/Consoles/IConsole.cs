namespace Game.Core.Consoles
{
    public interface IConsole
    {
        ConsoleColor ForegroundColor
        { get; set; }

        void Clear();

        void Write(string str);

        void ReadKey();

        string ReadLine();

        void WriteLine(string line);
    }
}
